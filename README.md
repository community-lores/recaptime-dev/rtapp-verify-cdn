# Profile Pic API for Verification Endpoint API

[![Netlify Status](https://api.netlify.com/api/v1/badges/a398bea8-594f-4854-8341-38dd8df2fd6e/deploy-status)](https://app.netlify.com/sites/rtapp-verify-cdn/deploys)

This repository hosts the static files and redirection stuff for the Verification Endpoint API integration.

## Available Endpoint Hosts

* `verify.cdn.rtapp.tk` and `verify-cdn.rtapp.tk`
* `rtapp-verify-cdn.netlify.app` - in case the whole `rtapp.tk` domain is literally down
